$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'user/user/list',
        datatype: "json",
        colModel: [			
			{ label: 'userId', name: 'userId', index: 'user_id', width: 50, key: true },
			{ label: '用户编号', name: 'userNumber', index: 'user_number', width: 80 }, 			
			{ label: '手机号', name: 'phone', index: 'phone', width: 80 }, 			
			{ label: '密码', name: 'password', index: 'password', width: 80 }, 			
			{ label: '盐', name: 'salt', index: 'salt', width: 80 }, 			
			{ label: '用户昵称', name: 'userName', index: 'user_name', width: 80 }, 			
			{ label: '真实姓名', name: 'realName', index: 'real_name', width: 80 }, 			
			{ label: '性别', name: 'sex', index: 'sex', width: 80 },
			{ label: '年龄', name: 'age', index: 'age', width: 80 }, 			
			{ label: '用户头像', name: 'picImg', index: 'pic_img', width: 80 }, 			
			{ label: '状态 0=冻结/1=正常', name: 'status', index: 'status', width: 80 }, 			
			{ label: '总金额', name: 'amount', index: 'amount', width: 80 }, 			
			{ label: '用户类型', name: 'userType', index: 'user_type', width: 80 }, 			
			{ label: '注册时间', name: 'regeistTime', index: 'regeist_time', width: 80 }, 			
			{ label: '创建者', name: 'createBy', index: 'create_by', width: 80 }, 			
			{ label: '修改时间', name: 'updateTime', index: 'update_time', width: 80 }, 			
			{ label: '修改人', name: 'updateBy', index: 'update_by', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
                el:'#rrapp',
                data:{
                    showList: true,
                    title: null,
                    user: {}
                },
                methods: {
                    query: function () {
                        vm.reload();
                    },
                    add: function(){
                        vm.showList = false;
                        vm.title = "新增";
                        vm.user = {};
                    },
                    update: function (event) {
                        var userId = getSelectedRow();
                        if(userId == null){
                            return ;
                        }
                        vm.showList = false;
                        vm.title = "修改";

                        vm.getInfo(userId)
		},
		saveOrUpdate: function (event) {
			var url = vm.user.userId == null ? "user/user/save" : "user/user/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.user),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var userIds = getSelectedRows();
			if(userIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "user/user/delete",
                    contentType: "application/json",
				    data: JSON.stringify(userIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(userId){
			$.get(baseURL + "user/user/info/"+userId, function(r){
                vm.user = r.user;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});