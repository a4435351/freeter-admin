package com.freeter.modules.apiUser.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.freeter.modules.apiUser.entity.UserEntity;

/**
 * 用户表
 * 
 * @author liuqi
 * @email 363236211@qq.com
 * @date 2018-06-07 13:18:33
 */
public interface UserDao extends BaseMapper<UserEntity> {
	
}
